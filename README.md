README for DMX Widgets
=======================

(c) Konrad Rosenbaum <konrad@silmor.de>, 2024
protected under the GNU GPL v.3 or at your option any newer
see [COPYING.txt](COPYING.txt) for details

This repository contains several hardware and firmware designs for USB connected DMX controller widgets.


Directories
-------------

modular_DMX_kit -> KiCad design for a modular test kit, not a usable widget, but a kit to develop one
